# README #
Módulo para assimilar o grupo que o usuário pertence, baseado no tamanho do campo taxvat.
### O que esperar desse módulo? ###
* Caso o usuário preencha com um CNPJ (14 caracteres), será assimilado ao grupo 2 (ou ID a definir conforme o sistema).
* Caso o usuário preencha com um CPF, será assimilado ao grupo 3 (ou ID a definir conforme o sistema).