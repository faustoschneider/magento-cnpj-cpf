<?php 

class Group1245_Cadastro_Model_Observer extends Mage_Core_Model_Abstract
{
	
	function saveGroupCustomer($observer)
	{

		Mage::log('customer_save_after observer init', null, 'g1245_cadastro.log');
		
		try {
			
			$customer = $observer->getCustomer();
			
			if(strlen(str_replace(array(".","/","-"), "", $customer->getTaxvat())) == 14)
				$customer->setData('group_id', 2); // pj
			else
				$customer->setData('group_id', 3); // pf

		} catch (Exception $e) {
			Mage::log("customer_save_after observer failed: " . $e->getMessage(), null, 'g1245_cadastro.log');
		}
		
		Mage::log('customer_save_after observer end', null, 'g1245_cadastro.log');

	}

}
